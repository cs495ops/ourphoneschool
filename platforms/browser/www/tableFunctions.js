function addCategories(query, table) {
    xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var data = this.responseText.split("<br>");
                    for (var x = 0; x < data.length - 1; x++) {
                        var row = document.createElement("tr");
                        var column = document.createElement("td");
                        var btn = document.createElement("input");
                        //var table = document.getElementById("categories");
                        var category = data[x];                        
                        btn.type = "button";
                        btn.id = category;
                        btn.value = category;
                        btn.value = "categoryButton";
                        column.appendChild(btn);
                        row.appendChild(column);
                        table.appendChild(row);
                        addClickEvents(category, "category");
                    } 
                }
            };
            xmlhttp.open("GET", "http://willshare.com/cs495/OurPhoneSchool/queryDatabase.php?query=" + query, true);
            xmlhttp.send();
}

function addClickEvent(btn, rows, title, type) {
    btn.addEventListener("click", function () {displayRows(rows, title, type)});
}

function displayRows(rows, title, type) {
    hideAllRows(tableRows);
    if (type == "category") {
        //Display subcategories
        title.innerHTML = "Subcategories";
        for (var i = 0; i < rows.length; i++) {
            rows[i].style.display = "";
        }
    } else if (type == "subcategory") {
        //Display courses
        title.innerHTML = "Courses";
        for (var i = 0; i < rows.length; i++) {
            rows[i].style.display = "";
        }
    } 
}

function hideAllRows(tableRows) {
    for (var i = 0; i < rows.length; i++) {
        rows[i].style.display = "none";
    }
}